InventoryManager = {
  storage = nil,
  ioPeripherals = {},
};

function InventoryManager:new(o, storage, ioPeripherals)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  self.storage = storage;
  self.ioPeripherals = ioPeripherals;

  return o;
end

function InventoryManager:transferItemTo(outputName, item, amount)
  local outputPeripheral = self:resolveOutputName(outputName);
  if not outputPeripheral then
    printError("Output not found");
    return 0, false;
  end

  if outputPeripheral.type == "chest" then
    return self:transferItemToChest(outputPeripheral.name, item, amount)
  elseif outputPeripheral.type == "inventory_manager" then
    remaining, ok = self:transferItemToChest(outputPeripheral.attachedChestName, item, amount)
    if not ok then
      return remaining, ok;
    end
    peripheral.wrap(outputPeripheral.name).addItemToPlayer(outputPeripheral.attachedChestPosition, amount, nil, item);
    return remaining, true;
  else
    printError("Output Type invalid")
    return 0, false;
  end
end

function InventoryManager:transferItemToChest(outputPeripheralName, item, amount)
  local inventories = self.storage.inventory[item];
  if not inventories then
    printError("Item not found");
    return amount, false;
  end

  for _, source in pairs(inventories) do
    local fetched = source.peripheral.pushItems(outputPeripheralName, source.slot, math.min(amount, source.count));
    amount = amount - fetched;
    if amount <= 0 then
      return 0, true;
    end
  end

  return amount, true;
end

function InventoryManager:emptyInputs()
  local totalSlotEmptied = 0;
  for _, input in pairs(self.ioPeripherals.inputs) do
    local doneSlots = self:emptyInput(input);
    if doneSlots > 0 then
      sleep(.5);
      totalSlotEmptied = totalSlotEmptied + doneSlots;
    end
  end
  return totalSlotEmptied;
end

function InventoryManager:emptyInput(input)
  if input.type ~= "chest" then
    printError("Invalid input type: "..input.type);
    return -1;
  end
  local inputPeripheral = peripheral.wrap(input.name);
  local doneSlots = 0;
  for slot, _ in pairs(inputPeripheral.list()) do
    local emptied = self:ingestSlotToExisting(inputPeripheral, slot)
    if not emptied then
      self:ingestSlotToEmpty(inputPeripheral, slot);
    end
    doneSlots = doneSlots + 1;
  end
  return doneSlots;
end

function InventoryManager:ingestSlotToExisting(sourcePeripheral, slot)
  local item = sourcePeripheral.getItemDetail(slot);
  if item == nil then
    printError("Fail to get item detail");
    return false;
  end
  local amount = item.count;
  local inventories = self.storage.inventory[item.name];

  if inventories == nil then
    return false;
  end

  for _, dest in pairs(inventories) do
    local itemLimit = dest.peripheral.getItemLimit(dest.slot);
    if itemLimit > dest.count then
      local amountToTransfer = math.min(itemLimit - dest.count, amount);
      local transfered = sourcePeripheral.pushItems(dest.peripheralName, slot, amountToTransfer);
      amount = amount - transfered;
      if amount <= 0 then
        return true;
      end
    end
  end

  return false;
end

function InventoryManager:ingestSlotToEmpty(sourcePeripheral, slot)
  local item = sourcePeripheral.getItemDetail(slot);
  if item == nil then
    return false;
  end
  local amount = item.count;

  for _, outputPeripheral in pairs(self.storage.peripheralsWithFreeSlots) do
    local transfered = sourcePeripheral.pushItems(outputPeripheral.name, slot, amount);
    amount = amount - transfered;
    if amount <= 0 then
      return true;
    end
  end
  return false;
end


function InventoryManager:resolveOutputName(name)
  local peripheral = self.storage.peripherals[name];
  if peripheral then
    return {
      type = "chest",
      name = name,
    };
  end

  local outputPeripheral = self.ioPeripherals.outputs[name];
  if outputPeripheral then
    return outputPeripheral;
  end

  return false;
end
