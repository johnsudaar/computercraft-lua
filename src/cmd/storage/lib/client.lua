local protocol = require("lib/protocol");

StorageControllerClient = {
  serverID = 0,
};

function StorageControllerClient:new(o, modemName)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  rednet.open(modemName);
  self:refreshServerID();

  return o;
end

function StorageControllerClient:refreshServerID()
  self.serverID = rednet.lookup(protocol.name);
end

function StorageControllerClient:availableItems()
  return self:sendAndWait(protocol.method_available_items);
end

function StorageControllerClient:requestItemTo(destination, name, amount)
  local args = { destination = destination, name = name, amount = amount};
  return self:sendAndWait(protocol.method_request_item_to, args);
end

function StorageControllerClient:sendAndWait(method, args)
  if not self.serverID then
    self:refreshServerID();
  end

  if not self.serverID then
    printError("Fail to lookup host")
    return false
  end

  rednet.send(self.serverID, self:buildMessage(method, args), protocol.name);

  for i = 1, 3, 1 do
    local result = self:waitForAnswer(method);
    if result then
      return result;
    end
  end
  printError("Failed to get answer, aborting...")
  return false;
end

function StorageControllerClient:waitForAnswer(method)
  local remoteID, messageStr = rednet.receive(protocol.name, 5);
  if not remoteID then
    printError("Message timeout");
    return false;
  end

  if remoteID ~= self.serverID then
    printError("Invalid remote ID");
    return false;
  end
  local message = textutils.unserialiseJSON(messageStr);
  if not message then
    printError("Invalid JSON received");
    return false;
  end

  if not message["method"] then
    printError("Invalid JSON structure");
    return false;
  end

  if message["method"] ~= method then
    printError("Invalid method");
    return false;
  end

  return message["result"];
end

function StorageControllerClient:buildMessage(method, args)
  local message = { method = method };
  if args then
    message["args"] = args;
  end
  return textutils.serialiseJSON(message);
end
