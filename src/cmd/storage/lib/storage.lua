Storage = {
  ioPeripherals = {},
  peripherals = {},
  inventory = {},
  peripheralsWithFreeSlots = {},
  totalSlots = 0,
  usedSlots = 0,
};

function Storage:new(o, ioPeripherals)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  self.ioPeripherals = ioPeripherals;
  return o;
end

function Storage:refreshPeripherals()
  local peripherals = {};
  for _, peripheralName in pairs(peripheral.getNames()) do
    if string.find(peripheralName, "minecraft:chest") then
      if not self:isPeripheralForStorage(peripheralName) then
        peripherals[peripheralName] = peripheral.wrap(peripheralName);
      end
    end
  end
  self.peripherals = peripherals;
end

function Storage:refreshInventory()
  local inventory = {};
  local peripheralsWithFreeSlots = {};
  local totalSlots = 0;
  local usedSlots = 0;
  for peripheralName, peripheral in pairs(self.peripherals) do
    local usedSlotsInPeripheral = 0;
    for slot, item in pairs(peripheral.list()) do
      usedSlotsInPeripheral = usedSlotsInPeripheral+ 1;
      local itemName = item["name"];
      local value = {
        peripheral = peripheral,
        peripheralName = peripheralName,
        slot = slot,
        count = item["count"],
      };

      if inventory[itemName] == nil then
        inventory[itemName] = { value };
      else
        table.insert(inventory[itemName], value);
      end
    end

    if usedSlotsInPeripheral < peripheral.size() then
      table.insert(peripheralsWithFreeSlots, { name = peripheralName });
    end
    usedSlots = usedSlots + usedSlotsInPeripheral;
    totalSlots = totalSlots + peripheral.size();
  end
  self.peripheralsWithFreeSlots = peripheralsWithFreeSlots;
  self.inventory = inventory;
  self.totalSlots = totalSlots;
  self.usedSlots = usedSlots;
end

function Storage:availableItems()
  local result = {};

  for itemName, itemSlots in pairs(self.inventory) do
    local total = 0;
    for _, itemSlot in pairs(itemSlots) do
      total = total + itemSlot.count;
    end
    result[itemName] = total;
  end
  return result;
end

function Storage:isPeripheralForStorage(name)
  if self:isPeripheralIn(self.ioPeripherals.outputs, name) then
    return true;
  end
  return self:isPeripheralIn(self.ioPeripherals.inputs, name);
end

function Storage:isPeripheralIn(list, name)
  for _, ignored in pairs(list) do
    if ignored.name == name then
      return true
    end

    if ignored.attachedChestName == name then
      return true;
    end
  end
  return false
end
