local protocol = {
  name = "storage_controller",
  method_available_items = "available_items",
  method_request_item_to = "request_item_to",
};

return protocol;
