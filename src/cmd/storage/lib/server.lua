local protocol = require("lib/protocol");

StorageControllerServer = {
  modemName = "",
  storage = nil,
  inventoryManager = nil,
};

function StorageControllerServer:new(o, modemName, hostname, storage, inventoryManager)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  self.modemName = modemName;
  self.storage = storage;
  self.inventoryManager = inventoryManager;

  rednet.open(modemName);
  rednet.host(protocol.name, hostname);

  return o;
end

function StorageControllerServer:listen()
  print("StorageControllerServer: Listening")
  while true do
    local remoteID, message = rednet.receive(protocol.name);
    if remoteID then
      print(string.format("[%d] Start", remoteID));
      self:handleConnection(remoteID, message);
      print(string.format("[%d] Done", remoteID));
    end
  end
end

function StorageControllerServer:handleConnection(remoteID, messageStr)
  local message = textutils.unserialiseJSON(messageStr)
  if not message then
    printError("Invalid JSON received:")
    printError(messageStr)
    return
  end

  local method = message["method"];
  if not method then
    printError("No method received")
    return
  end
  print(string.format("[%d] %s", remoteID, method));

  if method == protocol.method_available_items then
    self:methodAvailableItems(remoteID);
  elseif method == protocol.method_request_item_to then
    self:methodRequestitemTo(remoteID, message["args"]);
  else
    printError("Invalid method name");
  end
end

function StorageControllerServer:methodAvailableItems(remoteID)
  self:answer(remoteID, protocol.method_available_items, self.storage:availableItems());
end

function StorageControllerServer:methodRequestitemTo(remoteID, args)
  print("Sending transfer order");
  self.inventoryManager:transferItemTo(args["destination"], args["name"], args["amount"]);
  self:answer(remoteID, protocol.method_request_item_to, "ok");
end

function StorageControllerServer:answer(remoteID, method, result)
  local message = { method = method, result = result};
  rednet.send(remoteID, textutils.serialiseJSON(message), protocol.name);
end
