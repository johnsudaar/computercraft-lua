local basalt = require "/src/vendor/basalt";
local coroutines = require '/src/launcher/coroutines'

require 'ui/inventory'
require 'lib/storage'
require 'lib/inventory_manager'
require 'lib/server'
local json = require "/src/vendor/json"

local ioFile = fs.open("/storage_ios.json", "r");
if ioFile == nil then
  printError("IO configuration not found");
  return;
end

local ioJSON = ioFile.readAll();
ioFile.close();

local ios = json.decode(ioJSON);

local inventoryScreen = ios.controller.screenPosition;
local modemName = ios.controller.modemPosition;
local hostname = ios.controller.hostname;

local storage = Storage:new(nil, ios);
storage:refreshPeripherals();
storage:refreshInventory();

local inventory = InventoryManager:new(nil, storage, ios);

local inventoryUI = InventoryUI:new(nil, basalt, storage, inventoryScreen);
inventoryUI:refreshUI();

local server = StorageControllerServer:new(nil, modemName, hostname, storage, inventory)

coroutines.add("storage_controller_refresh", function ()
  while true  do
    sleep(1);
    storage:refreshPeripherals();
    storage:refreshInventory();
    inventoryUI:refreshUI();
  end
end);

coroutines.add("storage_controller_screen", function ()
  basalt.autoUpdate();
end)

coroutines.add("storage_controller_network", function ()
  server:listen();
end)

coroutines.add("storage_controller_empty", function ()
  while true do
    local totalSlotEmptied = inventory:emptyInputs();
    if totalSlotEmptied > 0 then
      print(string.format("Slots emptied: %d", totalSlotEmptied));
    end
    sleep(1);
  end
end)

coroutines.run();

