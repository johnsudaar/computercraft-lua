local coroutines = require '/src/launcher/coroutines'
local basalt = require "/src/vendor/basalt";
local json = require "/src/vendor/json"

require 'ui/request';
require 'lib/client';

local clientFile = fs.open("/storage_client.json", "r");
if clientFile == nil then
  printError("Client configuration not found");
  return;
end

local clientJSON = clientFile.readAll();
clientFile.close();

local clientConfig = json.decode(clientJSON);

local requestUI = RequestUI:new(nil, basalt);
requestUI:init();
local client = StorageControllerClient:new(nil, clientConfig.modem.position);

coroutines.add("storage_client_screen", function ()
  basalt.autoUpdate();
end)

coroutines.add("storage_client_refresh_items", function ()
  while true do
    sleep(1);
    local items = client:availableItems();
    if items then
      requestUI:loadItems(items);
    end
    sleep(10);
  end
end)

coroutines.add("storage_client_requests", function ()
  while true do
    local event, item, amount = os.pullEvent("storage_controller_request");
    client:requestItemTo(clientConfig.destination, item, amount);
    requestUI:resetUI();
  end
end)


coroutines.run();
