require "/src/vendor/basalt"

RequestUI = {
  basalt = nil,
  mainFrame = nil,
  searchBox = nil,
  amountBox = nil,
  suggestionContainer = nil,
  selectedItem = "",
  selectedItemIndex = 1,
  matchingItems = {},
  availableItems = {},
  pendingRequest = false,
};

function RequestUI:new(o, basalt)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  self.basalt = basalt;

  self.mainFrame = self.basalt.createFrame();
  self.mainFrame:setTheme({ FrameText = colors.white });

  self.searchBox = self.mainFrame:addInput():setInputType("text"):setPosition(1,1):setSize("parent.w", 1);
  self.suggestionContainer = self.mainFrame:addScrollableFrame():setSize("parent.w", "parent.h - 3"):setPosition(1, 2);
  local amountLabel = self.mainFrame:addLabel():setText("Amount: "):setPosition(1, "parent.h - 1");
  self.amountBox = self.mainFrame:addInput():setInputType("number"):setPosition(amountLabel:getWidth(), "parent.h - 1")
  self.amountBox:setSize(self.mainFrame:getWidth() - amountLabel.getWidth() + 1, 1)
  self.statusLabel = self.mainFrame:addLabel():setPosition(1, "parent.h"):setSize("parent.w", 1):setText("Loading...")

  return o;
end

function RequestUI:init()
  local onSearchBoxChange = function ()
    self.selectedItemIndex = 1;
    self:refreshSuggestions();
  end

  local onSearchBoxKey = function (_, event, key)
    if #self.matchingItems == 0 then
      return
    end

    if key == keys.up then
      self:moveSelection(-1);
      return
    elseif key == keys.down then
      self:moveSelection(1);
      return
    elseif key == keys.enter or key == keys.tab then
      self.amountBox:setFocus();
      return false
    end
  end

  local onAmountBoxKey = function (_, event, key)
    if key == keys.tab then
      self.searchBox:setFocus()
      return
    end
    if key ~= keys.enter then
      return
    end

    if self.pendingRequest then
      self:setStatus("Request in progress. Wait.")
      return false;
    end

    if self.selectedItem == "" then
      self:setStatus("No item selected");
      return false;
    end

    local amount = self.amountBox:getValue();
    if amount == "" then
      self:setStatus("Invalid amount");
      return false;
    end

    self.pendingRequest = true;
    self:setStatus("Request in progress.")
    os.queueEvent("storage_controller_request", self.selectedItem, amount)
    return false;
  end

  self.searchBox:onChange(onSearchBoxChange);
  self.searchBox:onKey(onSearchBoxKey);
  self.amountBox:onKey(onAmountBoxKey);

  self.searchBox:setFocus(true);
end

function RequestUI:resetUI()
  self.pendingRequest = false;
  self.searchBox:setValue("");
  self.amountBox:setValue("");
  self.searchBox:setFocus(true);
  self.selectedItemIndex = 1;
  self:refreshSuggestions();
  self:setStatus("Ready.");
end

function RequestUI:setStatus(message)
  self.statusLabel:setText(message)
end

function RequestUI:loadItems(items)
  self.availableItems = items;
  table.sort(self.availableItems);
  self:setStatus("Ready.")
  self:refreshSuggestions();
end

function RequestUI:refreshSuggestions()
  self.matchingItems = {};
  self.selectedItem = "";

  self.suggestionContainer:removeChildren();

  local searchValue = self.searchBox:getValue();

  local i = 1;
  for itemName, itemCount in pairs(self.availableItems) do
    if string.find(itemName, searchValue) then
      table.insert(self.matchingItems, itemName);

      local itemLabel = self.suggestionContainer:addLabel():setPosition(1, i);
      local itemText = string.format("%6d - %s", itemCount, itemName);
      if i == self.selectedItemIndex then
        self.selectedItem = itemName;
        itemLabel:setText("> " .. itemText);
      else
        itemLabel:setText("  " .. itemText);
      end
      i = i + 1;
    end
  end

  if #self.matchingItems == 0 then
    self:setStatus("No match");
  end
end

function RequestUI:moveSelection(delta)
  self.selectedItemIndex = self.selectedItemIndex + delta;
  self.selectedItemIndex = math.max(1, self.selectedItemIndex);
  self.selectedItemIndex = math.min(#self.matchingItems, self.selectedItemIndex);
  if self.selectedItemIndex >= self.suggestionContainer:getHeight() then
    self.suggestionContainer:setOffset(0, self.selectedItemIndex - self.suggestionContainer:getHeight());
  end
  self:refreshSuggestions();
end
