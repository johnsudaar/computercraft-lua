require "/src/vendor/basalt"
InventoryUI = {
  storage = nil,
  basalt = nil,
  mainFrame = nil,
  itemList = nil,
  slotLabel = nil,
  slotPB = nil,
  itemLength = 2,
  scrollToBottom = true,
};

function InventoryUI:new(o, basalt, storage, monitor)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  self.basalt = basalt;
  self.storage = storage;
  self.mainFrame = basalt.addMonitor()
  self.mainFrame:setMonitor(peripheral.wrap(monitor));
  self:buildUI();
  self:playScrollAnimation();

  return o;
end

function InventoryUI:buildUI()
  self.mainFrame:setTheme({FrameBG = colors.black, FrameText = colors.lightGray});
  self.itemList = self.mainFrame:addScrollableFrame():setSize("parent.w", "parent.h-2");
  self.slotLabel= self.mainFrame:addLabel():setPosition("parent.w/2 - self.w/2", "parent.h-1");
  self.slotPB = self.mainFrame:addProgressbar():setSize("parent.w", 1):setPosition(1, "parent.h"):setProgressBar(colors.green):setDirection("right");
end

function InventoryUI:refreshUI()
  self.itemLength = 2;
  self.itemList:removeChildren();
  for itemName, itemCount in pairs(self.storage:availableItems()) do
    self.itemList:addLabel():setPosition(1, self.itemLength):setText(string.format("%6d - %s", itemCount, itemName));
    self.itemLength = self.itemLength + 1;
  end

  local slotsUsagePercent = self.storage.usedSlots * 100 / self.storage.totalSlots;
  self.slotPB:setProgress(slotsUsagePercent);
  self.slotLabel:setText(string.format("Slots: %s/%s (%d%%)", self.storage.usedSlots, self.storage.totalSlots, slotsUsagePercent));

  self:playScrollAnimation();
end

function InventoryUI:playScrollAnimation()
  local _, curY = self.itemList:getOffset();
  local sizeY = self.itemList:getHeight();
  local totalY = self.itemLength - sizeY;
  local targetY = 0;

  -- If the target o the animation is less than 0, we do not need to animage, exit early
  if totalY < 0 then
    return
  end

  if curY == 0 then
    self.scrollToBottom = true;
  end

  if curY == totalY then
    self.scrollToBottom = false;
  end

  if self.scrollToBottom then
    targetY = totalY;
  else
    targetY = 0;
  end

  self.itemList:animateOffset(0, targetY, math.abs(curY-targetY)/2, 0, "linear", function ()
    self:playScrollAnimation();
  end);
end
