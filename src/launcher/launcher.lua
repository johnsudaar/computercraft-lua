require "utils/logger";

local coroutines = require "launcher/coroutines"
local state = require "utils/state"
local updater = require "updater/updater"

local launcher = {
  status = "booting",
  crashedSince = 0,
  log = Logger:new(o, {}),
};

function launcher.watchDog()
  while true do
    sleep(10)
    if not (coroutines.isRunning("main_loop") and coroutines.isRunning("program_loop")) then
      launcher.log:error("WATCHDOG: CONTROL LOOP DEAD: REBOOTING!!!")
      sleep(3)
      os.reboot()
    end
  end
end

function launcher.mainLoop(client, config)
  updater.init()
  version = updater.version
  launcher.log:info("Starting main loop. Version: "..version)
  local turtleID = config["turtle"]["id"]
  local turtleName = config["turtle"]["name"]
  launcher.log:info("Continuing with turtle ID:"..turtleID.. " Name: ".. turtleName)
  os.setComputerLabel(turtleName)

  while true do
    launcher.log:info("[" .. os.date() .. "] Refreshing state")

    local currentState = state.compute(config, version, launcher.status)

    launcher.log:debug("Sending update")
    local answer = client:update(turtleID, currentState)
    updater.tick()

    launcher.log:debug("Done, waiting for next loop")
    sleep(30)
  end
end

function launcher.runProgram(cmdline)
  shell.run(cmdline)
end

function launcher.programLoop(cmdline)
  coroutines.add("program", launcher.runProgram, cmdline)
  while true do
    sleep(6)
    if not coroutines.isRunning("program") then
      launcher.crashedSince = launcher.crashedSince + 1
      launcher.log:info("Program crashed (".. launcher.crashedSince.. "/10)")
      launcher.status = "crashed"
    else
      launcher.crashedSince = 0
      launcher.status = "running"
    end

    if launcher.crashedSince >= 10 then
      launcher.crashedSince = 0
      launcher.log:info("Restarting " .. cmdline)
      coroutines.add("program", launcher.runProgram, cmdline)
    end
  end
end

function launcher.start(client, config)
  coroutines.log = launcher.log;
  updater.log = launcher.log;

  coroutines.add("main_loop", launcher.mainLoop, client, config);
  coroutines.add("program_loop", launcher.programLoop, config["cmdline"]);
  coroutines.add("watchdog", launcher.watchDog);

  coroutines.run();
end

return launcher
