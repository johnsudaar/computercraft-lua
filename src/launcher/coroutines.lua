require "/src/utils/logger";

local coroutines = {
  threads = {},
  log = Logger:new(nil, {}),
}

function coroutines.add(name, func, ...)
  args = arg or {}
  local thread = coroutine.create(func)
  local ok, result = coroutine.resume(thread, table.unpack(arg))
  if not ok then
    coroutines.log:error("Fail to run ".. name .. ": ".. result)
    return false
  end
  table.insert(coroutines.threads, { name = name, thread = thread, filter = result})
end

function coroutines.isRunning(name)
  for i = #coroutines.threads, 1, -1 do
    if name == coroutines.threads[i].name then
      return true
    end
  end
  return false
end

function coroutines.run()
  while #coroutines.threads > 0 do
    local event = table.pack(os.pullEventRaw())
    for i = #coroutines.threads, 1 , -1 do
      if coroutines.threads[i].filter == nil or coroutines.threads[i].filter == event[1] or event[1] == 'terminate' then
        local ok, result = coroutine.resume(coroutines.threads[i].thread, table.unpack(event))
        if ok then
          if coroutine.status(coroutines.threads[i].thread)== "dead" then
            coroutines.log:info("Death of ".. coroutines.threads[i].name)
            table.remove(coroutines.threads, i)
          else
            coroutines.threads[i].filter = result
          end
        else
          coroutines.log:error("Fail to resume coroutine: ".. result.. " removing it from tracked routines")
          table.remove(coroutines.threads, i)
        end
      end
    end
  end
end

return coroutines
