require "utils/logger";

local updater = {
  versionPath = "/src/version",
  versionURL = "https://gitlab.com/johnsudaar/computercraft-lua/-/raw/master/src/version",
  version = "",
  tickCount = 0,
  checkEvery = 10,
  log = Logger:new(nil, {});
}

function updater.init()
  local versionFile = fs.open(updater.versionPath, "r")
  local version = versionFile.readAll()
  versionFile.close()

  updater.version = version
end

function updater.remoteVersion()
  local response = http.get {
    url = updater.versionURL,
    timeout = 10,
  }
  if response == nil then
    updater.log:error("Fait to find version")
  end
  remoteVersion = response.readAll()
  response.close()

  return remoteVersion
end

function updater.isUpToDate()
  local remote = updater.remoteVersion()
  return remote == updater.version
end

function updater.run()
  fs.copy("/src/install.lua", "/install.lua")
  startup = fs.open("/startup.lua", "w")
  startup.write("shell.run(\"/install.lua\")")
  startup.close()
  os.reboot()
end

function updater.tick()
  updater.tickCount = updater.tickCount + 1
  if updater.tickCount < updater.checkEvery then
    return
  end
  updater.tickCount = 0

  updater.log:debug("Checking for update...")
  if updater.isUpToDate() then
    updater.log:debug("Up to date.")
    return
  end
  updater.log:info("New version released. Updating...")
  sleep(2)
  updater.run()
end

return updater
