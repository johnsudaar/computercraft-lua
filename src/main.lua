require "utils/api"
require "utils/utils"
require "utils/logger"
debug = require "utils/debug"
configurator = require "utils/configurator"
launcher = require "launcher/launcher"

local config = configurator.get();

local log = Logger:new(nil, config.log);
launcher.log = log;

local client = APIClient:new(nil, config["api"]["user"], config["api"]["password"], config["api"]["url"], log)

if config["turtle"] == nil or config["turtle"]["id"] == nil then
  print("First run of this turtle. Registering it...")

  local turtle = client:register({name = config["turtle"]["name"]})
  if turtle ~= nil then
    config["turtle"]["id"] = turtle["id"]
    configurator.write(config)
  end
end

launcher.start(client, config)

print("Proccess ended!")
