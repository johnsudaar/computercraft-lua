function os.getComputerType()
  local ret = {}

  if term.isColor() then
    table.insert(ret, "advanced")
  end
  if pocket then
    table.insert(ret, "pda")
  elseif turtle then
    table.insert(ret, "turtle")
  else
    table.insert(ret, "computer")
  end
  return table.concat(ret, "_")
end
