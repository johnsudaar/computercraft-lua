json = require "vendor/json"

local configurator = {
  filePath = "/config.json"
}

function configurator.read()
  local configFile = fs.open(configurator.filePath, "r")
  if configFile == nil then
    return nil
  end

  local configJSON = configFile.readAll();
  configFile.close()
  local config = json.decode(configJSON)
  return config
end

function configurator.write(config)
  local configFile = fs.open(configurator.filePath, "w")
  local configJSON = json.encode(config)
  configFile.write(configJSON)
  configFile.close()
end

function configurator.get()
  local config = configurator.read()
  if config == nil then
    config = configurator.askFromUser()
    configurator.write(config)
  end
  local config = configurator.read()
  return config
end

function configurator.askFromUser()
  print("This turtle is not configured. Configuring it...")
  write("API Server: ")
  local url = read()

  write("API User: ")
  local user = read()

  write("API Token: ")
  local password = read("*")

  write("Turtle Name: ")
  local name = read()

  write("Cmd: ")
  local cmd = read()

  config = {
    api = {
      user = user,
      password = password,
      url = url,
    },
    turtle = {
      name = name,
    },
    cmdline = cmd,
  }
  return config
end

return configurator
