Logger = {
  file = nil,
  silent = false,
  level = "info",
  levels = {
    debug = 1,
    info = 2,
    error = 3,
  }
};

function Logger:new(o, config)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  config = config or {};

  if config.file then
    self.file = config.file;
  end
  if config.output then
    self.output = peripheral.wrap(config.output);
  end

  if config.silent then
    self.silent = true;
  end

  if config.level then
    self.level = config.level;
  end

  return o;
end

function Logger:info(msg)
  if not self:shouldLog("info") then
    return;
  end
  self:write(colors.white, string.format("[I] %s", msg));
end

function Logger:debug(msg)
  if not self:shouldLog("debug") then
    return;
  end
  self:write(colors.lightGray, string.format("[D] %s", msg));
end

function Logger:error(msg)
  if not self:shouldLog("error") then
    return;
  end
  self:write(colors.red, string.format("[E] %s", msg))
end

function Logger:shouldLog(level)
  return self.levels[level] >= self.levels[self.level];
end

function Logger:write(color, msg)
  if self.file then
    local file = fs.open(self.file, "a");
    file.writeLine(msg);
    file.close();
  end

  if self.silent then
    return;
  end

  local previousColor = nil;
  if term.isColour() then
    previousColor = term.getTextColor();
    term.setTextColor(color)
  end
  print(msg);
  if previousColor ~= nil then
    term.setTextColor(previousColor);
  end
end
