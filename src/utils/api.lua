local base64 = require'vendor/base64'
local json = require'vendor/json'

APIClient = {
  basic_auth = "",
  url = "",
  log = nil,
}

function APIClient:new(o, user, token, url, log)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;
  self.basic_auth = "Basic " .. base64.encode(user .. ":" .. token);
  self.url = url;
  self.log = log;
  return o
end

function APIClient:register(turtle)
  local body = json.encode(turtle);
  local url = self.url .. "/turtles.json";
  local response = http.post(url, body, {
    ["Content-Type"] = "application/json",
    ["Authorization"] = self.basic_auth,
  })

  if response == nil then
    self.log:error("Register request failed");
    return nil
  end

  local data = response.readAll()
  response.close()
  return json.decode(data)
end

function APIClient:update(turtleID, params)
  local body = json.encode(params)
  local url = self.url .. "/turtles/".. turtleID .. ".json"

  local response = http.request {
    url = url,
    body = body,
    headers = {
      ["Content-Type"] = "application/json",
      ["Authorization"] = self.basic_auth,
    },
    method = "PATCH"
  }

  if not response then
    self.log:error("Update request failed")
    return false
  end

  local ok, res = self:waitForEvent(url)
  if not ok then
    self.log:error("Update request error: " .. res)
    return false
  end

  res.close()
  return true
end

function APIClient:waitForEvent(url)
  while true do
    local event = {os.pullEvent()}
    local eventName = event[1]
    if eventName == "http_success" and event[2] == url then
      local handle = event[3]
      return true, handle
    end
    if eventName == "http_failure" and event[2] == url then
      return false, event[3]
    end
  end
end
