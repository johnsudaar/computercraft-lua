local state = {}

function state.compute(config, version, state)
  s = {
    computer_type = os.getComputerType(),
    version = version,
    name = config["turtle"]["name"],
    status = state,
    cmdline = config["cmdline"],
  }

  if turtle ~= nil then
    s["fuel_remaining"] = turtle.getFuelLevel()
  end

  local location = gps.locate()
  if location ~= nil then
    s["x"] = location.x
    s["y"] = location.y
    s["z"] = location.z
  end

  return s
end

return state
