#!/bin/bash

set -e

rm -r emulator/computer/0/src
cp -r src emulator/computer/0/
cp install.lua emulator/computer/0/src/
