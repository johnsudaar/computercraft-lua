# Changelog
## Next Release

* Remove extra debug logs in Storage Controller

## 0.0.5

* Add missing configuratio to storage controller
## 0.0.4

* Initial release of the storage server
* Add logging configuration
* Fix turtle status not being sent back
* Add timeout on update checks
* Fix close all http responses
* Fix filter not saved for coroutines
* Ask user for cmdline and do not loose turtle name on register

## 0.0.3

* Add multiprocess support and launch programs
* Send cmdline to the status API
* Fix typo in `os.getComputerType()`
* Set computer label on boot

## 0.0.2
* Improve logs in state updater

## 0.0.1
* Initial Release
