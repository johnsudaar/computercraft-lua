url = "https://gitlab.com/johnsudaar/computercraft-lua/-/archive/master/computercraft-lua-master.tar"
shell.run("wget https://raw.githubusercontent.com/MCJack123/CC-Archive/master/tar.lua")
tar = require("tar")

print("Dowloading initial code")
response = http.get(url)
result = response.readAll()
response.close()

file = fs.open("/archive.tar", "wb")
file.write(result)
file.close()

print("Extracting...")
data = tar.load("/archive.tar")
tar.extract(data, "/")
fs.delete("/src")
fs.delete("/update")
fs.move("/computercraft-lua-master/src/", "/src")
fs.move("/computercraft-lua-master/install.lua", "/src/install.lua")

print("Cleanup...")
fs.delete("/computercraft-lua-master")
fs.delete("/archive.tar")
fs.delete("/install.lua")
fs.delete("/tar.lua")

print("Adding startup file")
startup = fs.open("startup.lua", "w")
startup.write("shell.run(\"/src/main.lua\")")
startup.close()

print("Done. Rebooting!")

sleep(1)
os.reboot()
